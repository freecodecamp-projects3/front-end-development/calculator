import React from "react";

import "./App.css";

const defaultState = {
  display: "0",
  currentlyEnteredNumber: "0",
  mathoperatorCanBeClicked: false,
  lastEnteredMathOperator: "",
  lastEnteredMathOperators: "",
  zeroCanBeClicked: false,
  decimalCountInCurrentNumber: 0,
  decimalCanBeClicked: true,
  firstUse: true,
};
const orderOfOperations = [
  ["x", "/"],
  ["+", "-"],
];
const calculatorFunctions = {
  "+": (a, b) => a + b,
  "-": (a, b) => a - b,
  x: (a, b) => a * b,
  "/": (a, b) => a / b,
};
class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = defaultState;
    this.appendToDisplay = this.appendToDisplay.bind(this);
    this.appendToCurrentlyEnteredNumber =
      this.appendToCurrentlyEnteredNumber.bind(this);
    this.handleClickedNumber = this.handleClickedNumber.bind(this);
    this.clear = this.clear.bind(this);
    this.changeLastCharInDisplay = this.changeLastCharInDisplay.bind(this);
    this.changeLastTwoMathOperators =
      this.changeLastTwoMathOperators.bind(this);
    this.calculate = this.calculate.bind(this);
  }

  handleClickedNumber(char) {
    if (char === ".") {
      if (
        this.state.decimalCountInCurrentNumber === 0 &&
        this.state.decimalCanBeClicked === true &&
        this.state.currentlyEnteredNumber
      ) {
        this.appendToCurrentlyEnteredNumber(char);
        this.appendToDisplay(char);
        this.setState({
          mathoperatorCanBeClicked: false,
          decimalCountInCurrentNumber: 1,
          zeroCanBeClicked: true,
          decimalCanBeClicked: false,
          lastEnteredMathOperator: "",
          lastEnteredMathOperators: "",
        });
      }

      return;
    } else if (char === "0") {
      if (
        this.state.zeroCanBeClicked &&
        this.state.currentlyEnteredNumber !== "0"
      ) {
        this.appendToCurrentlyEnteredNumber(char);
        this.appendToDisplay(char);
        this.setState({
          mathoperatorCanBeClicked: true,
          lastEnteredMathOperator: "",
          lastEnteredMathOperators: "",
        });
      }
    } else if (char !== "0" && char !== ".") {
      if (
        this.state.display !== "0" &&
        this.state.currentlyEnteredNumber === "0"
      ) {
        return;
      }
      this.appendToCurrentlyEnteredNumber(char);
      this.appendToDisplay(char);
      this.setState({
        mathoperatorCanBeClicked: true,
        zeroCanBeClicked: true,
        decimalCanBeClicked: true,
        lastEnteredMathOperator: "",
        lastEnteredMathOperators: "",
      });
    }
  }
  handleClickecMathOperator(char) {
    if (this.state.lastEnteredMathOperator && char !== "-") {
      this.changeLastCharInDisplay(char);
      const mathOperators = this.state.lastEnteredMathOperators + char;
      this.setState({
        mathoperatorCanBeClicked: false,
        currentlyEnteredNumber: "",
        decimalCountInCurrentNumber: 0,
        lastEnteredMathOperator: char,
        lastEnteredMathOperators: mathOperators,
      });
      return;
    } else if (this.state.mathoperatorCanBeClicked) {
      this.appendToDisplay(char);
      const mathOperators = this.state.lastEnteredMathOperators + char;
      this.setState({
        mathoperatorCanBeClicked: false,
        currentlyEnteredNumber: "",
        decimalCountInCurrentNumber: 0,
        lastEnteredMathOperator: char,
        lastEnteredMathOperators: mathOperators,
      });
      return;
    } else if (this.state.lastEnteredMathOperator === "-" && char === "-") {
      this.appendToDisplay(char);
      this.setState({
        mathoperatorCanBeClicked: false,
        currentlyEnteredNumber: "",
        decimalCountInCurrentNumber: 0,
        lastEnteredMathOperator: "--",
        lastEnteredMathOperators: "--",
      });
      return;
    } else if (char === "-" && this.state.lastEnteredMathOperator) {
      this.appendToDisplay(char);
      this.setState({
        mathoperatorCanBeClicked: false,
        currentlyEnteredNumber: "",
        decimalCountInCurrentNumber: 0,
        lastEnteredMathOperator: "",
        lastEnteredMathOperators: "",
      });
      return;
    } else {
      this.changeLastTwoMathOperators(char);
      this.setState({
        mathoperatorCanBeClicked: false,
        currentlyEnteredNumber: "",
        decimalCountInCurrentNumber: 0,
        lastEnteredMathOperator: char,
        lastEnteredMathOperators: char,
      });
    }
  }
  appendToCurrentlyEnteredNumber(char) {
    if (this.state.firstUse && char !== "." && char !== "0") {
      this.setState({
        currentlyEnteredNumber: char,
        firstUse: false,
      });
      return;
    }
    const newCurrentlyEnteredNumber =
      this.state.currentlyEnteredNumber.concat(char);
    this.setState({
      currentlyEnteredNumber: newCurrentlyEnteredNumber,
      firstUse: false,
    });
  }
  appendToDisplay(char) {
    if (this.state.firstUse && char !== "." && char !== "0") {
      this.setState({
        display: char,
        firstUse: false,
      });
      return;
    }

    const newDisplay = this.state.display.concat(char);
    this.setState({
      display: newDisplay,
      firstUse: false,
    });
  }
  changeLastCharInDisplay(char) {
    const display = this.state.display;
    const slicedDisplay = display.slice(0, -1);
    const displayWithNewChar = slicedDisplay + char;
    this.setState({ display: displayWithNewChar });
  }
  changeLastTwoMathOperators(char) {
    const display = this.state.display;
    const slicedDisplay = display.slice(0, -2);
    const displayWithNewChar = slicedDisplay + char;
    this.setState({ display: displayWithNewChar });
  }
  clear() {
    this.setState(defaultState);
  }
  calculate() {
    let formula = this.state.display;
    for (let operation of orderOfOperations) {
      let operationRegExp = new RegExp(
        `\\-?[0-9]+\\.?[0-9]*[${operation[0]}${operation[1]}]{1}\\-?[0-9]+\\.?[0-9]*`
      );
      let matchedOperation = formula.match(operationRegExp);

      while (matchedOperation) {
        let result = this.returnResultOfOperation(matchedOperation[0]);

        formula = formula.replace(matchedOperation[0], result);

        matchedOperation = formula.match(operationRegExp);
      }
    }
    this.setState({
      display: formula,
      currentlyEnteredNumber: formula,
      mathoperatorCanBeClicked: true,
      lastEnteredMathOperator: "",
      zeroCanBeClicked: true,
      decimalCountInCurrentNumber: 0,
      decimalCanBeClicked: true,
    });
  }
  returnResultOfOperation(operation) {
    let operationArray = this.createArrayFromOperation(operation);

    let mathOperator = operationArray[1];
    let number1 = parseFloat(operationArray[0]);
    let number2 = parseFloat(operationArray[2]);

    const result = calculatorFunctions[mathOperator](number1, number2);
    return result;
  }

  createArrayFromOperation(operation) {
    let operationString = operation;
    const number = new RegExp("([-]?[0-9]+\\.?[0-9]*)");
    const number1 = operationString.match(number)[0];
    operationString = operationString.replace(number1, "");
    const mathOperator = operationString[0];
    operationString = operationString.replace(mathOperator, "");
    const number2 = operationString;

    return [number1, mathOperator, number2];
  }

  render() {
    return (
      <div id="calculator">
        <div id="display">{this.state.display}</div>
        <div id="inputs">
          <div id="left-panel">
            <div className="row">
              <p
                id="decimal"
                className="number"
                onClick={() => {
                  this.handleClickedNumber(".");
                }}
              >
                .
              </p>
              <p
                id="zero"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("0");
                }}
              >
                0
              </p>
            </div>
            <div className="row">
              <p
                id="one"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("1");
                }}
              >
                1
              </p>
              <p
                id="two"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("2");
                }}
              >
                2
              </p>
              <p
                id="three"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("3");
                }}
              >
                3
              </p>
            </div>
            <div className="row">
              <p
                id="four"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("4");
                }}
              >
                4
              </p>
              <p
                id="five"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("5");
                }}
              >
                5
              </p>
              <p
                id="six"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("6");
                }}
              >
                6
              </p>
            </div>
            <div className="row">
              <p
                id="seven"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("7");
                }}
              >
                7
              </p>
              <p
                id="eight"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("8");
                }}
              >
                8
              </p>
              <p
                id="nine"
                className="number"
                onClick={() => {
                  this.handleClickedNumber("9");
                }}
              >
                9
              </p>
            </div>
          </div>
          <div id="right-panel">
            <div className="row">
              <p id="equals" className="btn" onClick={this.calculate}>
                =
              </p>
            </div>
            <div className="row">
              <p
                id="add"
                className="btn"
                onClick={() => {
                  this.handleClickecMathOperator("+");
                }}
              >
                &#43;
              </p>
              <p
                id="subtract"
                className="btn"
                onClick={() => {
                  this.handleClickecMathOperator("-");
                }}
              >
                &#45;
              </p>
            </div>
            <div className="row">
              <p
                id="multiply"
                className="btn"
                onClick={() => {
                  this.handleClickecMathOperator("x");
                }}
              >
                X
              </p>
              <p
                id="divide"
                className="btn"
                onClick={() => {
                  this.handleClickecMathOperator("/");
                }}
              >
                /
              </p>
            </div>
            <div className="row">
              <p id="clear" className="btn" onClick={this.clear}>
                C
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Calculator;
